package com.neovisionaries.ws.client;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Holds the reference for a ScheduledExecutorService in use by the PeriodicalFrameSender
 *
 * @author Laurelianae
 */
class TimerUtils {

    private static ScheduledExecutorService executorService;

    public static ScheduledExecutorService getExecutorService() {
        return executorService;
    }

    /**
     * Used to set a different executor in case the default settings arent fitting.
     * @param executor ScheduledExecutorService to use
     */
    public static void setExecutorService(ScheduledExecutorService executor) {
        executorService = executor;
    }

    static {
        executorService = Executors.newScheduledThreadPool(20, new LoomFactory());
    }

    private static class LoomFactory implements ThreadFactory {

        private final AtomicInteger threadId = new AtomicInteger(0);
        @Override
        public Thread newThread(Runnable r) {
            return Thread.ofVirtual().name("VThread-" + threadId.incrementAndGet()).unstarted(r);
        }
    }
}
