/*
 * Copyright (C) 2017 Neo Visionaries Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.neovisionaries.ws.client;

/**
 * Modified by Laurelianae at 26.05.2021
 */
abstract class WebSocketThread implements Runnable
{
    protected final WebSocket mWebSocket;
    private final ThreadType mThreadType;
    private final String mName;
    private final Thread mThread;


    WebSocketThread(String name, WebSocket ws, ThreadType type)
    {
        mWebSocket  = ws;
        mThreadType = type;
        mName       = name;
        mThread     = Thread.ofVirtual().name(mName).unstarted(this);
    }

    public String getName() {
        return mName;
    }

    @Override
    public void run()
    {
        ListenerManager lm = mWebSocket.getListenerManager();

        if (lm != null)
        {
            // Execute onThreadStarted() of the listeners.
            lm.callOnThreadStarted(mThreadType, Thread.currentThread());
        }

        runMain();

        if (lm != null)
        {
            // Execute onThreadStopping() of the listeners.
            lm.callOnThreadStopping(mThreadType, Thread.currentThread());
        }
    }

    public void start(){
        mThread.start();
    }

    public Thread getThread(){
        return mThread;
    }

    public void interrupt(){
        mThread.interrupt();
    }

    public boolean isInterrupted(){
        return mThread.isInterrupted();
    }


    public void callOnThreadCreated()
    {
        ListenerManager lm = mWebSocket.getListenerManager();

        if (lm != null)
        {
            lm.callOnThreadCreated(mThreadType, Thread.currentThread());
        }
    }


    protected abstract void runMain();
}
